package grammar;

import driver.Driver;
import grammar.TablaSimbolos.idType;
import grammar.TablaSimbolos.MyTable.Row;
import grammar.tableSLR.movType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

import lexer.Lexer;
import lexer.Token;
import semantico.SemanticAction;
import semantico.SemanticSymbol;

public class SLR {
	public CanonicoLR canonico;
	private LinkedList<Integer> pila;
	private LinkedList<ElementoPila> simbolos;
	private LinkedList<Token> input;
	private ArrayList<Move> used;
	private ArrayList<Token> errors;
	public static TablaSimbolos tabla;
	
	private Terminal token;
	private Token current;
	
	public Arbol arbolSintactico;	//Arbol sintactico
	private LinkedList<Nodo> pilaNodos;	//pila del arbol sintáctico

	private ElementoPila lastPop;
	
	public SLR() {
		this.canonico = new CanonicoLR();
		SLR.tabla = new TablaSimbolos();
        SemanticAction.tabla = SLR.tabla;
		this.pila = new LinkedList<Integer>();
		this.simbolos = new LinkedList<ElementoPila>();
		this.input = new LinkedList<Token>();
		this.used = new ArrayList<Move>();
		this.errors = new ArrayList<Token>();
		this.arbolSintactico = new Arbol();
		this.pilaNodos = new LinkedList<Nodo>();
		lastPop = new ElementoPila((Symbols)Non_terminal.array);
	}
	
	public void readToken(Lexer lexer) throws IOException{	//Método de prueba que solo lee e imprime los tokens
		while(true){
			current = lexer.yylex();
            token = current.token;         
            if(token !=null){
           	System.out.println("Token_>"+token.toString());
            }else{
            	break;
            }
		}
	}
	
	public void analyzePerToken2(Lexer lexer) throws IOException{	//Parser, recibe el lexer
		input.add(new Token("$",Terminal.$,-1,-1));
		pila.push(0);
		int w = 0;
        LinkedList<SemanticSymbol> semanticS = new LinkedList<SemanticSymbol>();
		//System.out.printf("%-40s %-40s %-40s\n","PILA","SIMBOLOS","ENTRADA");
		while(true){
			current = lexer.yylex(); //Leer token
	        if(current !=null){	            
	        	token = current.token;
	        	if(token == Terminal.M_COMMENT || token == Terminal.COMMENT)
	            	continue;	            	
	        	input.removeLast();	//Agregar el token a la cola de simbolos, al final siempre poner $
	            input.add(current);
	            input.add(new Token("$",Terminal.$,-1,-1));         
	            
	        }
		if(w < 35){	
			//System.out.println("\n");
			//System.out.printf("%-40s %-40s %-40s\n",pila.toString(),simbolos.toString(),input.get(0).value.toString());
			
			w+=1;
		}
			
	        
	        Token tt;
			Symbols ss;
			if(input.isEmpty()){//Si ya no hay input, tomar solo el $
				ss = Terminal.$;
				tt = new Token("$",Terminal.$,-1,-1);
			}
			else{
				tt = input.peek();	//Sino tomar el valor de la cabeza de la cola
				ss = tt.token;
			}
			
			if(pila.isEmpty()){
				if(ss.equals(Terminal.$)){
					System.out.println("Análisis terminado");
					System.exit(0);
				}
				else{
					pila.push(0);
					if(this.errors.contains(tt))
						input.pop();
					continue;
				}
			}
			if(ACTIONtype(pila.peek(),ss).equals(movType.shift)){	//Si es un shift
				pila.push(ACTION(pila.peek(),ss));
				if(!ss.equals(Terminal.$)){
//-------------------------Rutina de simbolos--------------------------------------------------------------
					if(ss.equals(Terminal.FUNCTION)){
						ElementoPila idFunc =  this.simbolos.peek();
						if(tabla.existsID(idFunc.name))
                            Driver.miVentana.PrintError("Warning: La funcion "+idFunc.name+" ya ha sido declarada");
						else
							SLR.tabla.saveID(idFunc.name, idType.funcion);
						SLR.tabla.addTable();
						Grammar.tabCnt++;
					}
//---------------------------------------------------------------------------------------------------------
					simbolos.push(new ElementoPila(ss,tt.value));
                    semanticS.push(new SemanticSymbol(ss, tt.line, tt.column));
                    semanticS.getFirst().SetName(tt.value);
                    input.pop();
				}
			}else if ((ACTIONtype(pila.peek(),ss) == movType.reduce)){	//Si es un reduce
				boolean passed = true;
                int index = ACTION(pila.peek(),ss);
				Production p = canonico.grammar.grammar.get(index);
                SemanticAction action = canonico.grammar.semantic.get(index);
				//System.out.printf("%5s", "reduce");
				ArrayList<ElementoPila> reducedTokens = new ArrayList<ElementoPila>();
				Symbols cancels[] = p.getProduction();
                SemanticSymbol tmpSSymbol = new SemanticSymbol(p.getLeftSide());
                LinkedList<SemanticSymbol> cancelSymbol = new LinkedList<SemanticSymbol>();
                int countCancelSymbol = 0;
				for(int i=cancels.length-1; i>=0; i--){
					if(simbolos.peek().sym.equals(cancels[i])){
                        ElementoPila tmpEl = simbolos.pop();
                        lastPop = tmpEl;
						reducedTokens.add(0, tmpEl);
                        countCancelSymbol++;
							pila.pop();
					}else{
						passed = false;
					}	
				}
							
				if(!passed){//Rutina de recuperación de errores
					if(ss.equals(Terminal.$))
						System.err.println("Error de reducción");
					else{
						if(!this.errors.contains(tt)){
                            Driver.miVentana.PrintError("Error encontrado en '"+tt.value+"' : linea "+(tt.line+1)+" columna "+(tt.column+1));
							this.errors.add(tt);
						}
					}
					boolean recovered = false;
					do{
						recovered = recoverRutine2(pila.peek(),tt, lexer);
						if(!recovered){
							pila.pop();
						}
					}while(!recovered && !pila.isEmpty());
					continue;
				}
				ElementoPila reduce = new ElementoPila(p.getLeftSide());
				if(passed)
					Grammar.codingRules.get(p).getCode(reducedTokens, reduce);
				simbolos.push(reduce);
				if(!ACTIONtype(pila.peek(),p.getLeftSide()).equals(movType.not)){
					pila.push(ACTION(pila.peek(),p.getLeftSide()));
				}
				
//--------------------------------Rutina de Árbol sintáctico--------------------------------------------------
/*
				Nodo nuevo = new Nodo(new ElementoPila(p.getLeftSide()),p);
				LinkedList<Nodo> nodos = new LinkedList<Nodo>();
				for(int i = 0; i<reducedTokens.size();i++)
					if(reducedTokens.get(i).sym.getClass().equals(grammar.Non_terminal.class))
						nodos.push(pilaNodos.pop());
				for(int i=0; i<reducedTokens.size();i++){
					if(reducedTokens.get(i).sym.getClass().equals(grammar.Terminal.class)){
						nuevo.addChildren(new Nodo(reducedTokens.get(i)));
					}else{
						nuevo.addChildren(nodos.pop());
					}
				}
				pilaNodos.push(nuevo);
*/
//------------------------------------------------------------------------------------------------------------

//--------------------------------Rutina de tabla de simbolos-------------------------------------------------
		//Si la reducción es de func_dec quiere decir que ya se acabó un bloque, entonces regresas el puntero de la tabla al padre
				if(p.getLeftSide().equals(Non_terminal.func_dec)){
					SLR.tabla.upTable();
					Grammar.tabCnt--;
					//*Agregar al id de la función que se redujo, el número de parámetros
				}
		//Si es un assignment, hay que agregar a la tabla de simbolos
				if(p.getLeftSide().equals(Non_terminal.assignment)){
					ElementoPila tmp = reducedTokens.get(0);
					if(SLR.tabla.existsID(tmp.name)){
						//System.err.println("El nombre '"+tmp.name+"' ya existe");
						//*Agregar cambio de tipo si es que lo hubiera
					}else{
						SLR.tabla.saveID(tmp.name,idType.var);
						//*Agregarle el tipo
					}		
				}
		//En caso que la reducción sea de un term
				if(p.getLeftSide().equals(Non_terminal.term)){
					//Si es un term -> ID, entonces agregar a la tabla de simbolos
					if(reducedTokens.get(0).sym.equals(Terminal.ID)){ 
						ElementoPila tmp = reducedTokens.get(0);
						if(SLR.tabla.existsID(tmp.name)){
							//System.err.println("El nombre '"+tmp.name+"' ya existe");
							//*Agregar cambio de tipo si es que lo hubiera
						}else{
							SLR.tabla.saveID(tmp.name,idType.var);
							//*Agregarle el tipo
						}
					}
		//Si es un term -> []
					else if(reducedTokens.get(0).sym.equals(Terminal.LCOR) && reducedTokens.get(1).sym.equals(Terminal.RCOR)){
						ElementoPila tmp = simbolos.get(2);
						
						if(tmp.sym.equals(Terminal.ID)){
							if(SLR.tabla.existsID(tmp.name)){
								//System.err.println("El nombre '"+tmp.name+"' ya existe");
								//*Agregar cambio de tipo si es que lo hubiera
							}else{
								SLR.tabla.saveID(tmp.name,idType.array,0);//Se pone cero porque es array vacío
								System.err.println("[]");
								//*Agregarle el tipo
							}
						}
					}
		//Si es un term -> [array]
					else if(reducedTokens.get(0).sym.equals(Terminal.LCOR) && reducedTokens.get(1).sym.equals(Non_terminal.array)){
						ElementoPila tmp = simbolos.get(2);
						System.err.println("[1,2,3]");
						if(tmp.sym.equals(Terminal.ID)){
							if(SLR.tabla.existsID(tmp.name)){
								//System.err.println("El nombre '"+tmp.name+"' ya existe");
								//*Agregar cambio de tipo si es que lo hubiera
							}else{
								SLR.tabla.saveID(tmp.name,idType.array,0); //En vez de 0, va el numero de elementos del array
								//*Agregarle el tipo
							}
						}
					}
				}
		//Si es un param_dec
				else if(p.getLeftSide().equals(Non_terminal.param_dec)){
					ElementoPila tmp;
					if(reducedTokens.get(0).sym.equals(Terminal.ID)){
						tmp = reducedTokens.get(0);
					}
					else
						tmp = reducedTokens.get(2);
					if(SLR.tabla.redeclaredParemeter(tmp.name))
                        System.err.println("Parametros repetidos");
					else{
						SLR.tabla.saveID(tmp.name,idType.parameter);
					}				
				}

                try {
                    if (action != null) action.Execute(semanticS, tmpSSymbol);
                    for(int i = 0; i < countCancelSymbol; i++) cancelSymbol.add(semanticS.pop());
                    tmpSSymbol.SetLine(cancelSymbol.getLast().GetLine());
                    tmpSSymbol.SetColumn(cancelSymbol.getLast().GetColumn());
                    semanticS.push(tmpSSymbol);
                }catch (Exception e){}

//-------------------------------------------------------------------------------------------------------------

			
			}else if ((ACTIONtype(pila.peek(),ss) == movType.acc)){		//Si llega al estado de aceptación
				System.out.println("\nAccepted\n\n");
				//SLR.tabla.printTablaSimbolos();
				break;
			}else{	//Rutina de recuperación de errores
				if(ss.equals(Terminal.$))
					System.err.println("Error de reducción");
				else{
					if(!this.errors.contains(tt)){
                        Driver.miVentana.PrintError("Error encontrado en '"+tt.value+"' : linea "+(tt.line+1)+" columna "+(tt.column+1));
						this.errors.add(tt);
					}
				}					
				boolean recovered = false;
				do{
					 recovered = recoverRutine2(pila.peek(),tt, lexer);
					if(!recovered){
						pila.pop();
					}
				}while(!recovered && !pila.isEmpty());			
			}			
		}
		
		//arbolSintactico.root= pilaNodos.pop(); //Asignar el nodo raiz al arbol
		//arbolSintactico.reducirArbol(arbolSintactico.root);	//Reduces el arbol para que quede sin tanta basura
		//arbolSintactico.recorrerArbolCodigo(arbolSintactico.root, 0); //Lo de arriba hacia abajo imprimes
        StringBuilder build = new StringBuilder();
		for (Map.Entry<String, Row> entry : SLR.tabla.current.tablita.entrySet()) {
			if(entry.getValue().identity.equals(idType.var)) {
                build.append("var ");
                build.append(entry.getKey());
                build.append(";\n");
            }
		}
        build.append("\n");
		while(!simbolos.isEmpty()){
            build.append(simbolos.pop().code);
		}
        Driver.miVentana.SetCode(build.toString());
        // Verificamos llamadas a funciones
        SemanticAction.funcCall.CheckFuncCalls();        
	}
	
	public int ACTION(int state, Symbols s){
			return canonico.table.getAction(state, s);
	}
	
	public movType ACTIONtype(int state, Symbols s){
			return canonico.table.getActionType(state, s);
	}

//----------------------Rutina de recuperación de errores-------------------------
	private boolean recoverRutine2(int current,Token in, Lexer lexer) throws IOException{
		int state = current;
		Non_terminal[] paths = Non_terminal.values();		
		LinkedList<Move> posibles = new LinkedList<Move>();		
		for(int i=0; i< paths.length; i++){
			int next = this.ACTION(state, paths[i]);
			if(next!=-9){
				posibles.add(new Move(next, new ElementoPila(paths[i])));
			}
		}		
		Token t1 = in;
		for(int i=0;i<posibles.size();i++){
			Move m = posibles.get(i);
			if(!used.contains(m)){
				ArrayList<Terminal> follow = this.canonico.grammar.FOLLOW.get(m.symToAdd.sym);
				if(follow.contains(t1.token)){
					used.add(m);				
					m.move();
					//input.add(t1);
					//input.add(new Token("$",Terminal.$,-1,-1));
					//input_rec.push(t1);
					return true;
				}
			}
					
		}
		return false;
	}
	
//---------------------------------Clase anidada---Elemento de recuperación de errores----------------------------		
	class Move{
		public int state;
		public ElementoPila symToAdd;
		public Move(int ste, ElementoPila s){
			state = ste;
			symToAdd = s;
		}
		
		public void move(){
			pila.push(state);
			simbolos.push(symToAdd);
		}
		
		public boolean equals(Object o){
			if(o.getClass() == Move.class){
				Move temp = (Move)o;
				if(this.state == temp.state)
					if(this.symToAdd.name == temp.symToAdd.name)
						if(this.symToAdd.sym == temp.symToAdd.sym)
							return true;
			}
			return false;
		}
	}
	
//---------------------------------Clase anidada---Elemento de la Pila de Simbolos-----------------------------	
	public class ElementoPila{
		public Symbols sym;
		public String name;
		public StringBuilder code;
		//Type
		
		public ElementoPila(Symbols s, String str){
			this.sym = s;
			this.name = str;
			this.code = new StringBuilder();
		}
		
		public ElementoPila(Symbols s){
			this.sym = s;
			this.name = "";
			this.code = new StringBuilder();
		}
		
		public String toString(){
			return sym.toString();	
		}
	}

}
