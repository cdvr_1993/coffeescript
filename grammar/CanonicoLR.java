package grammar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class CanonicoLR {
	
	public Grammar grammar;
	public ArrayList<State> canonico;
	public tableSLR table;
	private boolean flag = true;
	private ArrayList<Production> done;
	
	public CanonicoLR(){
		this.grammar = new Grammar(Non_terminal.stalist0,Non_terminal.stalist);
		this.canonico = new ArrayList<State> ();
		this.table = new tableSLR();
		done = new ArrayList<Production>();
		this.fillCanonico();
	}
	
	public State Closure(Production production){
		Production p = null;
		Symbols s = null;
		ArrayList<Production> sub = null;			//Array de producciones
		ArrayList<Production> stateProductions = new ArrayList<Production> (); //Producciones de la cerradura
		stateProductions.add(production); //Agregar la primer producción
		for(int i=0; i< stateProductions.size();i++){ //Iterar en el conjunto de producciones de la cerradura
			p = stateProductions.get(i); 	//Producción actual
			if(p.isAllRead())				//Si ya está leida, pasar a la siguiente producción
				continue;
			s = p.getSymbolAtTransition();	//Sino, obtener el símbolo que se está leyendo
			if(s.getClass() == Non_terminal.class){ 	//Si el símbolo es un no terminal, entonces
				sub = this.grammar.getProductions((Non_terminal)s);	//Obtener todas las producciones de la gramática con ese símbolo a la izquierda
				for(int j=0; j<sub.size();j++){						//Iterar sobre esas producciones
					if(!stateProductions.contains(sub.get(j)))		//Si no están contenidas en la cerradura, 
						stateProductions.add(sub.get(j));			//entonces agregarlas		
				}
			}	
		}
		return new State(stateProductions);	//Regresar la cerradura
	}
	
	public void goTo(State I, Symbols x){
		Production p;
		Symbols s;
		boolean change = false;
		State currentState =I;			//Estado actual
		int indexCurrentState = this.canonico.indexOf(currentState); //Obtener índice del estado
		ArrayList<Production> stateProductions = currentState.rules; //Obtener las producciones del estado
		for(int i=0; i< stateProductions.size();i++){ //Iterar sobre las producciones
			p = stateProductions.get(i).clone();	//Clonar la producción actual
			if(p.isAllRead()){		//Si ya está leída, pasar a la siguiente producción
				continue;
			}		
			s = p.getSymbolAtTransition();	//SIno, obtener el símbolo a leer en esa producción
			if(s.equals(x)){	//Si el símbolo es igual al input X
				p.addTransition();	//Entonces, agregar transición ala producción
				if (!currentState.exists(x)){	//Si en el estado actual, no hay aún una transición con ese símbolo	
					for(int j=0; j<this.canonico.size();j++){ //Iterar sobre todo el canónico
						if(this.canonico.get(j).rules.get(0).equals(p)){	//Si la primera producción de algún estado es igual a la producción actual
							currentState.addTransition(x, j); //Agregar entonces una transición del estado actual a ese estado destino
							this.table.addElement(indexCurrentState, x, j, true); //Agregar un shift a la tabla
							change = true;
						}
					}
					if(!change){ //Si no se encontró ningún estado, entonces crear un nuevo estado
						State newState = Closure(p); //Obtener el nuevo estado con Closure de la producción actual
						this.canonico.add(newState); //Agregar el nuevo estado al canónico
						this.table.addRow();	//Agregar una fila en la tabla
						currentState.addTransition(x, this.canonico.indexOf(newState)); //Agregar la transición
						this.table.addElement(indexCurrentState, x, this.canonico.indexOf(newState), true); //Agregar shift a la tabla
						this.flag = true;
					}
				}else{	//Si ya había una transición con ese símbolo
					State transState = this.canonico.get(currentState.getTransition(x));	//Se obtiene el estado al que se dirije la transición
					if(!transState.rules.contains(p)){	//Si ese estado no contiene a P
						ArrayList<Production> newProd = Closure(p).rules;	//Entonces, obtener todas las reglas del closure de P
						for(int n=0; n<newProd.size();n++){	//Iterar sobre esas reglas
								if(!transState.containsProduction(newProd.get(n))){	//Si no están contenidas en el estado,		
									transState.rules.add(newProd.get(n));		//Entonces,agregar la producción al estado destino.
									this.flag = true;
								}
							
						}
					}
				}
			}
		}
	}
	
	public void fillCanonico(){
		State current; 				//Estado actual sobre el que se está trabajando
		ArrayList <Symbols> syms;	//Símbolos que deben leerse en el estado actual
		ArrayList <Production> ends;//Producciones del estado actual que ya fueron leídas completamente. Reducciones
		this.canonico.add(Closure(this.grammar.getInitialProduction())); //Hace el estado0:  S'-> S
		this.table.addRow(); //Se agrega una nueva fila a la tabla LSR
		do{ //Iterar hasta que ya no se haga ningún cambio
			this.flag = false;
		for(int i=0; i<this.canonico.size();i++){ //Iterar sobre todos los estados del canónico
			current = this.canonico.get(i);			//Obtener estado actual
			if(current.isReductionState()){			//Si hay alguna reducción que hacerse
				ends= current.getReadProduction();	//Obtiene producciones que deben reducirse
				for(int j=0;j<ends.size();j++){		//Iterar sobre esas producciones
					Production temp = ends.get(j).clone();	//Clonar la producción a reducir
					temp.resetTransition();			//Poner a cero las transiciones 
					if(!containsDone(temp)){
						done.add(temp);
					}
					if(temp.equals(this.grammar.grammar.get(0))){	///Si la transicion es el estado inici
						this.table.addElement(i, Terminal.$, -1, true); //Se agrega elemento de "accept" a la tabla
					}
					else{ //Sino
						ArrayList<Terminal> follow = this.grammar.FOLLOW.get(temp.getLeftSide()); //Se obtiene el follow del no terminal de la producción
						for(int m=0;m<follow.size();m++){ //Iterar sobre el follow
							this.table.addElement(i, (Symbols)follow.get(m), this.grammar.getIndex(temp), false); //Agregar reducción a la tabla
						}
					}
					
				}
				
			}
			
			syms = current.getSymbolsToRead();		//Obtener símbolos que no se han leido
				for(int k=0;k<syms.size();k++){		
					this.goTo(current, syms.get(k));	//Hacer función ir_A para cada una de esos símbolos.
				}
		}
		}while(flag); 
	}

	public boolean containsDone(Production p){
		for(int i=0; i<done.size();i++){
			if(done.get(i).getLeftSide() == p.getLeftSide())
				if(done.get(i).getProduction()== p.getProduction())
						return true;
		}
		return false;
	}
	
	public void printCanonico(){
		for(int i=0; i<this.canonico.size();i++){
			State state = this.canonico.get(i);
			System.out.println("I"+i+" trans: "+state.transitions.toString());
			for(int j=0;j<state.rules.size();j++)
				state.rules.get(j).printProduction();
		}
		System.out.println("Grammar: "+this.grammar.grammar.size());
		System.out.println("Prods: "+this.done.size());
		for(int i=0;i<done.size();i++)
			done.get(i).printProduction();
	}
}
