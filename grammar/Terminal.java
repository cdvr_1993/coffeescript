package grammar;

import lexer.Token;

public enum Terminal implements Symbols{

IF, ELSE, UNLESS, THEN, FOR, WHILE, IN, WHEN, INT_DIVIDE,
POW, TRUE_MOD, EQUAL, N_EQUAL, LESS, LESS_EQ, GREATER, GREATER_EQ,
ASSIGN, PLUS_AS, MINUS_AS, TIMES_AS, DIVIDE_AS, LPAR, RPAR, END,
COMMA, PLUS, MINUS, TIMES, DIVIDE, AND, OR, NOT, MOD, COMMENT, RETURN,
M_COMMENT, RAN_3, RAN_2, LCOR, RCOR, EXIST, LKEY, RKEY, DOT, SWITCH,
NUMBER, TRUE, FALSE, STRING, ID, FUNCTION, ENDS, ERROR, DOT_COMMA, CHAR,
$;

	
	
	//TIMES,LPAR, RPAR,PLUS,ID,$;
	//q,n,t,u,r,$;

	@Override
	public Symbols getSymbol(String s) {
		Symbols sym;
		try{
			sym = Terminal.valueOf(s);
		}catch(Exception e){
			sym = null;
		}
		return sym;
	}

	@Override
	public int getTotal() {
		// TODO Auto-generated method stub
		return 57;
	}
	
}
