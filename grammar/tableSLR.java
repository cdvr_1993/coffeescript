package grammar;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class tableSLR {
	enum movType{shift,reduce,move,acc,not};
	
	public ArrayList<ArrayList <simbolo>> table;
	
	public tableSLR(){
		table = new ArrayList<ArrayList <simbolo>>();
	}
	
	public void addRow(){
		table.add(new ArrayList<simbolo>());
	}
	
	public void addElement(int state, Symbols s, int val, boolean shift){
		if(!this.contains(state, s)){
			simbolo new_sim=null;
			if(val==-1){
				System.out.println("$");
				new_sim = new simbolo(s,val, movType.acc);
			}else{
				if (s.getClass().equals(Terminal.class)){
					if(shift)
						new_sim = new simbolo(s,val, movType.shift);
					else
						new_sim = new simbolo(s,val, movType.reduce);
				}
				if (s.getClass()== Non_terminal.class){
					new_sim = new simbolo(s,val, movType.move);
				}
			}		
			this.table.get(state).add(new_sim);
			
		}
	}
	
	public boolean contains(int state, Symbols s){
		ArrayList<simbolo> temp = this.table.get(state);
		for(int i=0; i< temp.size();i++){
			if(temp.get(i).sym == s){
				//System.out.println("ya existe");
				return true;
			}
		}	
		return false;
	}
	
	public int getAction(int state, Symbols s){
		ArrayList<simbolo> temp = this.table.get(state);
		for(int i=0; i< temp.size();i++){
			if(temp.get(i).sym == s){
				return temp.get(i).value;
			}
		}	
		return -9;
	}
	
	public movType getActionType(int state, Symbols s){
		ArrayList<simbolo> temp = this.table.get(state);
		for(int i=0; i< temp.size();i++){
			if(temp.get(i).sym.equals(s)){
				return temp.get(i).type;
			}
		}	
		return movType.not;
	}

	public String strType(movType t){
		if(t == movType.acc)
			return "acc";
		else if(t == movType.reduce)
			return "r";
		else if(t == movType.shift)
			return "s";
		else
			return "";
	}
		
	public void simplePrint(){
		ArrayList<simbolo> temps = new ArrayList<simbolo>();
		for (int i=0; i<table.size();i++){
			temps = table.get(i);
			System.out.printf("%1d",i);
			for(int j=0; j<temps.size();j++){
				simbolo s = temps.get(j);
				String str = s.sym.toString()+"->"+this.strType(s.type)+s.value;
				System.out.printf("%25s  ",str);
			}
			System.out.println("\n");
		}
	}
	
//-------------------CLASE SIMBOLO : clase anidada -----------------------
	class simbolo{
		public Symbols sym;
		public int value;
		public movType type;
		
		public simbolo(Symbols s, int val, movType t){
			this.sym = s;
			this.value = val;
			this.type = t;
		}
		
		public boolean equals(Symbols obj){
			if(this.getClass() == obj.getClass()){
				simbolo comp = (simbolo)obj;
				if(this.sym.getClass() == comp.sym.getClass()){
					if(this.sym.equals(comp.sym)){
						return true;
					}
				}
			}
			return false;
		}
	}	
}
