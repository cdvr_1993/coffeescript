package grammar;

public enum Non_terminal implements Symbols{
stalist0,
stalist, statement, assignment, val, expr_bool, expr_bool1, expr_bool2, equality, equality1,
equal_op, bool, end, func_dec, param_list, if_s, statement_in_line, if_line, param_dec,
comparison_op, expr_math, expr_math1, expr_math2, term, switch_s, switch_body, function_call,
cycle, for_s, while_s, unless, rango, cycle_line, array, hash, stalist_line, assign_op, return_s;
	
	
	//S0,S,P,M,E,K,;
	//E0,E,T,F;
	@Override
	public Symbols getSymbol(String s) {
		Symbols sym;
		try{
			sym = Non_terminal.valueOf(s);
		}catch(Exception e){
			sym = null;
		}
		return sym;
	}

	@Override
	public int getTotal() {
		// TODO Auto-generated method stub
		return 38;
	}
}
