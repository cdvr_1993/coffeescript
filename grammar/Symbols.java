package grammar;

public interface Symbols {
	
	public Symbols getSymbol(String s);
	
	public int getTotal();
	
}
