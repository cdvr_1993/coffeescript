package grammar;
import semantico.Code;
import semantico.SemanticAction;
import semantico.SemanticSymbol;
import grammar.SLR.ElementoPila;
import grammar.TablaSimbolos.idType;
import grammar.TablaSimbolos.MyTable.Row;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

public class Grammar {
	private Non_terminal original;	// Origen: Es el S'
	private Non_terminal init;		// Es el no terminal inicial: S
	public ArrayList<Production> grammar;	//Gramática. Array de producciones
    public ArrayList<SemanticAction> semantic;
	public HashMap <Non_terminal,ArrayList<Terminal>> FIRST;	//términos FIRST de cada no terminal
	public HashMap <Non_terminal,ArrayList<Terminal>> FOLLOW;	//términos FOLLOW de cada no terminal
	
	public static HashMap <Production, Code> codingRules = new HashMap<Production, Code>();
	public static int tabCnt = 0;
	public static int CycleCnt = 0;
	
	public Grammar(Non_terminal initial, Non_terminal original){
		this.init = initial;
		this.original = original;
		this.grammar = new ArrayList<Production>();
        this.semantic = new ArrayList<SemanticAction>();
		this.fillGrammar();			//LLena el array de la grmática con las producciones
		this.FIRST = new HashMap <Non_terminal,ArrayList<Terminal>>();
		this.fillFIRST();			//Calcula el FIRST
		this.FOLLOW = new HashMap <Non_terminal,ArrayList<Terminal>>();
		this.fillFOLLOW();			//Calcula el FOLLOW
		//System.out.println("FIRST: "+this.FIRST.toString()+"\n");
		//System.out.println("FOLLOW: "+this.FOLLOW.toString()+"\n");

	}
	
	public Production getInitialProduction(){		//Método para obtener la producción inicial S' -> S
		ArrayList<Production> initialProduction = this.getProductions(this.init);
		return initialProduction.get(0);
	}
	
	public ArrayList<Production> getProductions(Non_terminal value){	//Obtener todas las producciones de un no terminal
		ArrayList<Production> list = new ArrayList<Production>();		
		for(int i=0; i<this.grammar.size();i++){
			if(this.grammar.get(i).getLeftSide().equals(value))
				list.add(this.grammar.get(i));
		}	
		return list;
	}
	
	public int getIndex(Production p){//Obtiene el índice de una producción dada
		int index=-1;
		boolean flag = false;
		for(int i=0; i<this.grammar.size();i++){
			if(p.getLeftSide()==grammar.get(i).getLeftSide()){
				if(p.getProduction().equals(grammar.get(i).getProduction())){
					index=i;
					flag=true;
				}
			}
			if(flag) break;
		}
		return index;
	}
	

	
	private boolean getFirst(Non_terminal term){ //Función FIRST para un no terminal
		boolean changed = false;
		ArrayList<Terminal> first = new ArrayList<Terminal>(); 		//Array first
		if(this.FIRST.containsKey(term))	//Si ya existe el no terminal en el hash
			first = this.FIRST.get(term);	//Agregar terminales a first
		ArrayList<Production> productions = this.getProductions(term);	//Obtener todas lasproducciones del no terminal
		for(int i=0;i<productions.size();i++){	//Iterar sobre esas producciones
			Symbols s = productions.get(i).getProduction()[0];		//Obtener el primer simbolo de la producción actual
			if(s.getClass()==Terminal.class ){				//Si es un terminal y no está contenido en first, entonces agregarlo
				if(!first.contains((Terminal)s)){
					first.add((Terminal)s);					
					changed = true;
				}
			}else{					//si es un no terminal
				if(this.FIRST.containsKey(s)){			
					ArrayList<Terminal> toAdd = this.FIRST.get(s); 	//Obtener el first de ese no terminal si es que ya fue calculado y agregarlo al first
					for(int n=0; n<toAdd.size();n++){
						if(!first.contains(toAdd.get(n))){
							first.add(toAdd.get(n));
							changed = true;
						}
					}
				}
			}
		}
		this.FIRST.put(term, first);	//Guardar first en FIRST
		return changed;
	}
	
	private void fillFIRST(){		//Función que llena el FIRST a partir del getFirst	
		Non_terminal[] terms = Non_terminal.values();	//Obtener todos los no terminales
		boolean loop = false;
		for(int i=0; i<terms.length;i++){	//Iterar sobre los no terminales
			if(terms[i]!= this.init){		//Si es diferente a S' -> S, entonces...
				boolean tmp = this.getFirst(terms[i]);	//Calcular el First de ese no terminal
				if(!loop)			
					loop = tmp;			//Si loop= false, entonces asignar el valor de retorno de getFirst (true si se agregaron elementos al FIRST)
				if(i==terms.length-1 && loop==true){//Se repite hasta que ya no haya cambios en el FIRST
					i=-1;
					loop = false;
				}
			}			
		}
	}
	
	private boolean getFollow(Non_terminal term){ //Función que obtiene el FOLLOW de un no terminal
		boolean changed = false;
		Production p;
		ArrayList<Terminal> follow = new ArrayList<Terminal>();	//follow
		if(this.FOLLOW.containsKey(term))		//Agregar elementos FOLLOW calculados previamente
			follow = this.FOLLOW.get(term);
		
		for(int i=0; i<this.grammar.size();i++){	//Iterar sobre las producciones de la gramática
			p=this.grammar.get(i);					//Obtener producción actual
			int index = p.contains(term);			//Ver si esa producción contiene el no terminal del follow
			if(index >=0){							//Si lo contiene
				if(index == (p.getProduction().length-1)){	//Si está al final de la producción
					if(this.FOLLOW.containsKey(p.getLeftSide())){	//Agregar follow del no terminal del lado izquierdo de la producción actual
						ArrayList<Terminal> toAdd = this.FOLLOW.get(p.getLeftSide());
						if(toAdd.size()<=0){
							getFollow(p.getLeftSide());
						}
						toAdd = this.FOLLOW.get(p.getLeftSide());
						for(int n=0; n<toAdd.size();n++){
							if(!follow.contains(toAdd.get(n))){
								follow.add(toAdd.get(n));
								changed = true;
							}
						}
					}
				}else{	//Sino, si el simbolo que le sigue....	
					Symbols next = p.getProduction()[index+1];
					if(next.getClass()==Terminal.class){		//...es un terminal
						if(!follow.contains((Terminal)next)){	//entonces agregar el terminal al follow
							follow.add((Terminal)next);
							changed=true;
						}
					}else{
						if(this.FIRST.containsKey(next)){		//....es un no terminal
							ArrayList<Terminal> first = this.FIRST.get(next); 	//Agregar el FIRST de ese no terminal al follow
							for(int n=0; n<first.size();n++){
								if(!follow.contains(first.get(n))){
									follow.add(first.get(n));
									changed = true;
								}
							}
						}
					}
				}
			}
		}
		this.FOLLOW.put(term, follow);	//Guardar el follow calculado en FOLLOW
		return changed;
	}
	
	private void fillFOLLOW(){ //Hace el follow
		Non_terminal[] terms = Non_terminal.values();	//Obtiene todos los no terminales
		ArrayList <Terminal> begin = new ArrayList <Terminal>();
		begin.add(Terminal.$);
		this.FOLLOW.put(this.original, begin); //Agregar al FOLLOW del primer no terminal, el $
		boolean loop = false;
		for(int i=0; i<terms.length;i++){ //Iterar sobre los no terminales
			if(terms[i]!= this.init){		//Calcular el follow hasta que ya no haya cambios en FOLLOW
				boolean tmp = this.getFollow(terms[i]);
				if(!loop)
					loop = tmp;
				if(i==terms.length-1 && loop==true){
					i=-1;
					loop = false;
				}
			}			
		}
	}


    //Método para agregar una producción a la gramática
    private void addProduction(Non_terminal left, Symbols[] rule){
        this.grammar.add(new Production(left,rule));
        // Para que vayan los índices acorde a las producciones agregadas
        this.semantic.add(null);
    }

    // Agregamos la acción semantica
    private void addProduction(Non_terminal left, Symbols[] rule, SemanticAction action){
        this.grammar.add(new Production(left, rule));
        this.semantic.add(action);
    }
    
    private void addProduction(Non_terminal left, Symbols[] rule,Code code){
        Production p = new Production(left, rule);
    	this.grammar.add(p);
    	this.semantic.add(null);
        Grammar.codingRules.put(p, code);
    }
    
    private void addProduction(Non_terminal left, Symbols[] rule, SemanticAction action, Code code){
        Production p = new Production(left, rule);
    	this.grammar.add(p);
        this.semantic.add(action);
        Grammar.codingRules.put(p, code);
    }

	private void fillGrammar(){ //Producciones de la gramática
		Code appendingCode = new Code(){
        	@Override
			public void getCode(ArrayList<ElementoPila> tokens, ElementoPila reduce){
        		for(int i=0; i<tokens.size();i++){
        			if(tokens.get(i).name!="")
        				reduce.code.append(tokens.get(i).name);
				}
			}
        	
        };
        
       
        Code upCode = new Code(){
        	@Override
			public void getCode(ArrayList<ElementoPila> tokens, ElementoPila reduce){
        		for(int i=0; i<tokens.size();i++)
        			reduce.code.append(tokens.get(i).code);
			}
        };
        
        Code upCode_Element = new Code(){
        	@Override
			public void getCode(ArrayList<ElementoPila> tokens, ElementoPila reduce){
        		reduce.code.append(tokens.get(0).code);
        		reduce.code.append(tokens.get(1).name);
			}
        };
        
        Code code_element_code = new Code(){
        	@Override
			public void getCode(ArrayList<ElementoPila> tokens, ElementoPila reduce){
        		reduce.code.append(tokens.get(0).code);
        		reduce.code.append(tokens.get(1).name);
        		reduce.code.append(tokens.get(2).code);
			}
        };
        
        Code element_code_element = new Code(){
        	@Override
			public void getCode(ArrayList<ElementoPila> tokens, ElementoPila reduce){
        		reduce.code.append(tokens.get(0).name);
        		reduce.code.append(tokens.get(1).code);
        		reduce.code.append(tokens.get(2).name);
			}
        };
		
		this.addProduction(Non_terminal.stalist0, new Symbols[]{Non_terminal.stalist},upCode);

        this.addProduction(Non_terminal.stalist, new Symbols[]{Non_terminal.statement, Non_terminal.stalist},upCode);
       
        this.addProduction(Non_terminal.stalist, new Symbols[]{Non_terminal.statement},upCode);
        
        
        this.addProduction(Non_terminal.statement, new Symbols[]{Non_terminal.statement_in_line, Terminal.DOT_COMMA, Terminal.ENDS},new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(0).code);
				reduce.code.append(tokens.get(2).name);
			}
        	
        });
        this.addProduction(Non_terminal.statement, new Symbols[]{Non_terminal.stalist_line, Terminal.ENDS},upCode_Element);
        
        this.addProduction(Non_terminal.statement, new Symbols[]{Non_terminal.statement_in_line, Terminal.ENDS},upCode_Element );
        this.addProduction(Non_terminal.statement, new Symbols[]{Non_terminal.func_dec}, upCode);
        this.addProduction(Non_terminal.statement, new Symbols[]{Non_terminal.if_s, Terminal.ENDS}, upCode_Element);
        this.addProduction(Non_terminal.statement, new Symbols[]{Non_terminal.switch_s, Terminal.ENDS},upCode_Element);
        this.addProduction(Non_terminal.statement, new Symbols[]{Non_terminal.cycle}, upCode);
        this.addProduction(Non_terminal.statement, new Symbols[]{Terminal.ENDS}, appendingCode);
        this.addProduction(Non_terminal.statement, new Symbols[]{Non_terminal.return_s}, upCode);

        this.addProduction(Non_terminal.statement_in_line, new Symbols[]{Non_terminal.assignment}, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(0).code+";");
				
			}
        	
        });
        this.addProduction(Non_terminal.statement_in_line, new Symbols[]{Non_terminal.expr_bool},new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(0).code+";");
				
			}
        	
        });
        this.addProduction(Non_terminal.statement_in_line, new Symbols[]{Non_terminal.cycle_line}, upCode);

        // Acción que se repite para las instrucciones aritméticas
        SemanticAction accionAritmeticaSubida = new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol right = simbolos.get(0);
                //System.out.println(insert + " -> " + right + " -> " + right.GetType());
                //System.out.println(right + " -> " + right.GetColumn());
                insert.SetType(right.GetType());
            }
        };

        this.addProduction(Non_terminal.assignment, new Symbols[]{Terminal.ID, Non_terminal.assign_op, Non_terminal.expr_bool}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(2), secondOp = simbolos.get(0);
                tabla.SetType(firstOp.GetName(), secondOp.GetType());
            }
        },new Code(){
        	@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(0).name+" ");
				reduce.code.append(tokens.get(1).code+" ");
				reduce.code.append(tokens.get(2).code);				
			}
        });

        this.addProduction(Non_terminal.assign_op, new Symbols[]{Terminal.ASSIGN}, appendingCode);
        this.addProduction(Non_terminal.assign_op, new Symbols[]{Terminal.EXIST,Terminal.ASSIGN}, appendingCode);
        this.addProduction(Non_terminal.assign_op, new Symbols[]{Terminal.PLUS_AS}, appendingCode);
        this.addProduction(Non_terminal.assign_op, new Symbols[]{Terminal.MINUS_AS}, appendingCode);
        this.addProduction(Non_terminal.assign_op, new Symbols[]{Terminal.TIMES_AS}, appendingCode);
        this.addProduction(Non_terminal.assign_op, new Symbols[]{Terminal.DIVIDE_AS}, appendingCode);

        SemanticAction accionIgualdad = new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                insert.SetType(TYPE.BOOLEAN);
            }
        };

        this.addProduction(Non_terminal.expr_bool, new Symbols[]{Non_terminal.expr_bool,Terminal.EQUAL, Non_terminal.expr_bool1},
                			accionIgualdad, new Code(){

								@Override
								public void getCode(
										ArrayList<ElementoPila> tokens,
										ElementoPila reduce) {
									reduce.code.append(tokens.get(0).code+" == "+tokens.get(2).code);
									
								}
        	
        });

        this.addProduction(Non_terminal.expr_bool, new Symbols[]{Non_terminal.expr_bool,Terminal.N_EQUAL, Non_terminal.expr_bool1},
                accionIgualdad, new Code(){

			@Override
			public void getCode(
					ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(0).code+" != "+tokens.get(2).code);
				
			}

});

        this.addProduction(Non_terminal.expr_bool, new Symbols[]{Non_terminal.expr_bool1}, accionAritmeticaSubida, upCode);

        // Acción para comprobar el AND y el OR
        SemanticAction accionArimeticaBooleana = new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(0), secondOp = simbolos.get(2);
                TYPE[] accepted = new TYPE[] {TYPE.BOOLEAN, TYPE.COMODIN};
                if (!ContainsType(accepted, firstOp))
                    PrintError(firstOp.toFormat() + " -> Se esperaba un booleano");
                if (!ContainsType(accepted, secondOp))
                    PrintError(secondOp.toFormat() + " -> Se esperaba un booleano");
                insert.SetType(TYPE.BOOLEAN);
            }
        };

        this.addProduction(Non_terminal.expr_bool1, new Symbols[]{Non_terminal.expr_bool1, Terminal.AND, Non_terminal.expr_bool2},
                accionArimeticaBooleana, new Code(){

					@Override
					public void getCode(ArrayList<ElementoPila> tokens,
							ElementoPila reduce) {
						reduce.code.append(tokens.get(0).code+" && "+tokens.get(2).code);					
					}
        		
        });

        this.addProduction(Non_terminal.expr_bool1, new Symbols[]{Non_terminal.expr_bool1, Terminal.OR, Non_terminal.expr_bool2},
                accionArimeticaBooleana, new Code(){

				@Override
				public void getCode(ArrayList<ElementoPila> tokens,
						ElementoPila reduce) {
					reduce.code.append(tokens.get(0).code+" || "+tokens.get(2).code);					
				}
		
        });

        this.addProduction(Non_terminal.expr_bool1, new Symbols[]{Non_terminal.expr_bool2}, accionAritmeticaSubida, upCode);

        this.addProduction(Non_terminal.expr_bool2, new Symbols[]{Terminal.NOT, Non_terminal.bool}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(0);
                if (!ContainsType(new TYPE[] {TYPE.BOOLEAN, TYPE.COMODIN}, firstOp))
                    PrintError(firstOp.toFormat() + " -> Se esperaba un booleano");
                insert.SetType(TYPE.BOOLEAN);
            }
        }, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,ElementoPila reduce) {
				reduce.code.append("!"+tokens.get(1).code);
				
			}
        	
        });
        this.addProduction(Non_terminal.expr_bool2, new Symbols[]{Non_terminal.bool}, accionAritmeticaSubida, upCode);

        SemanticAction accionTipoBooleana = new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                insert.SetType(TYPE.BOOLEAN);
            }
        };

        this.addProduction(Non_terminal.bool, new Symbols[]{Terminal.TRUE}, accionTipoBooleana, new Code(){
            @Override
            public void getCode(ArrayList<ElementoPila> tokens,
                                ElementoPila reduce) {
                reduce.code.append("true");

            }
        });
        this.addProduction(Non_terminal.bool, new Symbols[]{Terminal.FALSE}, accionTipoBooleana, new Code(){
            @Override
            public void getCode(ArrayList<ElementoPila> tokens,
                                ElementoPila reduce) {
                reduce.code.append("false");

            }
        });
        this.addProduction(Non_terminal.bool, new Symbols[]{Non_terminal.equality}, accionAritmeticaSubida, upCode);

        this.addProduction(Non_terminal.equality, new Symbols[]{Non_terminal.equality, Non_terminal.comparison_op, Non_terminal.expr_math},
                new SemanticAction() {
                    @Override
                    public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                        SemanticSymbol firstOp = simbolos.get(0), secondOp = simbolos.get(2);
                        TYPE[] accepted = new TYPE[] {TYPE.NUMBER, TYPE.COMODIN};
                        if (!ContainsType(accepted, firstOp))
                            PrintError(firstOp.toFormat() + " -> Se esperaba un numero");
                        if (!ContainsType(accepted, secondOp))
                            PrintError(secondOp.toFormat() + " -> Se esperaba un numero");
                        // Aunque se muestre error, se le asigna valor númerico para que no genere errores en cascada
                        insert.SetType(TYPE.BOOLEAN);
                    }
                }, upCode);

        this.addProduction(Non_terminal.equality, new Symbols[]{Non_terminal.expr_math}, accionAritmeticaSubida, upCode);

        // Son solo operadores por lo tanto no les asignamos reglas semánticas
        this.addProduction(Non_terminal.comparison_op, new Symbols[]{Terminal.LESS}, appendingCode);
        this.addProduction(Non_terminal.comparison_op, new Symbols[]{Terminal.LESS_EQ}, appendingCode);
        this.addProduction(Non_terminal.comparison_op, new Symbols[]{Terminal.GREATER}, appendingCode);
        this.addProduction(Non_terminal.comparison_op, new Symbols[]{Terminal.GREATER_EQ}, appendingCode);

        this.addProduction(Non_terminal.expr_math, new Symbols[]{Non_terminal.expr_math, Terminal.PLUS,Non_terminal.expr_math1},
                new SemanticAction() {
                    @Override
                    public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                        SemanticSymbol firstOp = simbolos.get(0), secondOp = simbolos.get(2);
                        TYPE[] accepted = new TYPE[] {TYPE.NUMBER, TYPE.STRING, TYPE.COMODIN};
                        if (!ContainsType(accepted, firstOp))
                            PrintError(firstOp.toFormat() + " -> Se esperaba un numero");
                        if (!ContainsType(accepted, secondOp))
                            PrintError(secondOp.toFormat() + " -> Se esperaba un numero");
                        // Aunque se muestre error, se le asigna valor númerico para que no genere errores en cascada
                        insert.SetType(TYPE.NUMBER);
                    }
                }, new Code(){

					@Override
					public void getCode(ArrayList<ElementoPila> tokens,
							ElementoPila reduce) {
						reduce.code.append(tokens.get(0).code+" + "+tokens.get(2).code);						
					}
                	
                });

        SemanticAction aritmethicOnlyNumbers = new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(0), secondOp = simbolos.get(2);
                TYPE[] accepted = new TYPE[] {TYPE.NUMBER, TYPE.COMODIN};
                if (!ContainsType(accepted, firstOp))
                    PrintError(firstOp.toFormat() + " -> " + secondOp.GetName() + " -> Se esperaba un numero");
                if (!ContainsType(accepted, secondOp))
                    PrintError(secondOp.toFormat() + " -> Se esperaba un numero");
                // Aunque se muestre error, se le asigna valor númerico para que no genere errores en cascada
                insert.SetType(TYPE.NUMBER);
            }
        };

        this.addProduction(Non_terminal.expr_math, new Symbols[]{Non_terminal.expr_math, Terminal.MINUS,Non_terminal.expr_math1},
                aritmethicOnlyNumbers,  new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(0).code+" - "+tokens.get(2).code);						
			}
        	
        });

        this.addProduction(Non_terminal.expr_math, new Symbols[]{Non_terminal.expr_math1}, accionAritmeticaSubida,upCode);

        this.addProduction(Non_terminal.expr_math1, new Symbols[]{Non_terminal.expr_math1, Terminal.TIMES,Non_terminal.expr_math2},
                aritmethicOnlyNumbers,  new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(0).code+" * "+tokens.get(2).code);						
			}
        	
        });

        this.addProduction(Non_terminal.expr_math1, new Symbols[]{Non_terminal.expr_math1, Terminal.DIVIDE,Non_terminal.expr_math2},
                aritmethicOnlyNumbers, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(0).code+" / "+tokens.get(2).code);						
			}
        	
        });

        this.addProduction(Non_terminal.expr_math1, new Symbols[]{Non_terminal.expr_math1, Terminal.INT_DIVIDE,Non_terminal.expr_math2},
                aritmethicOnlyNumbers, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append("Math.floor("+tokens.get(0).code+"/"+tokens.get(2).code+")");						
			}
        	
        });

        this.addProduction(Non_terminal.expr_math1, new Symbols[]{Non_terminal.expr_math2}, accionAritmeticaSubida, upCode);

        this.addProduction(Non_terminal.expr_math2, new Symbols[]{Non_terminal.expr_math2, Terminal.POW,Non_terminal.term},
                aritmethicOnlyNumbers,  new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append("Math.pow("+tokens.get(0).code+","+tokens.get(2).code+")");						
			}       	
        });

        this.addProduction(Non_terminal.expr_math2, new Symbols[]{Non_terminal.expr_math2, Terminal.MOD,Non_terminal.term},
                aritmethicOnlyNumbers, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(0).code+" % "+tokens.get(2).code);						
			}
        	
        });

        this.addProduction(Non_terminal.expr_math2, new Symbols[]{Non_terminal.expr_math2, Terminal.TRUE_MOD,Non_terminal.term},
                aritmethicOnlyNumbers, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append("("+tokens.get(0).code+" % "+tokens.get(2).code+" + "+tokens.get(2).code+") % "+tokens.get(2).code);						
			}
        	
        });

        this.addProduction(Non_terminal.expr_math2, new Symbols[]{Non_terminal.term}, accionAritmeticaSubida, upCode);

        this.addProduction(Non_terminal.term, new Symbols[]{Terminal.NUMBER},new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                insert.SetType(TYPE.NUMBER);
            }
        }, appendingCode);

        this.addProduction(Non_terminal.term, new Symbols[]{Terminal.STRING}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                insert.SetType(TYPE.STRING);
            }
        }, appendingCode);

        SemanticAction accionTipoComodin = new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                insert.SetType(TYPE.COMODIN);
            }
        };

        this.addProduction(Non_terminal.term, new Symbols[]{Terminal.ID}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(0);
                if (!tabla.existsID(firstOp.GetName()))
                    PrintError(firstOp.toFormat() + " -> No existe el identificador \"" + firstOp.GetName() + "\"");
                if (firstOp.GetType() == null)
                    insert.SetType(TYPE.COMODIN);
                else
                    insert.SetType(tabla.GetType(firstOp.GetName()));
            }
        }, appendingCode);

        this.addProduction(Non_terminal.term, new Symbols[]{Non_terminal.function_call}, accionTipoComodin, upCode);

        this.addProduction(Non_terminal.term, new Symbols[]{Terminal.LPAR,Non_terminal.expr_bool, Terminal.RPAR}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol right = simbolos.get(1);
                insert.SetType(right.GetType());
            }
        },new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(1).code);
				
			}
        	
        });

        this.addProduction(Non_terminal.func_dec, new Symbols[]{Terminal.ID,Terminal.FUNCTION,Terminal.LPAR,Terminal.RPAR,Terminal.ENDS,Non_terminal.stalist, Terminal.END},
                new SemanticAction() {
                    @Override
                    public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                        SemanticSymbol firstOp = simbolos.get(6);
                        //System.out.println(tabla.existsID(firstOp.GetName()));
                        tabla.SetCount(firstOp.GetName(), 0);
                        //System.out.println(tabla.GetCount(firstOp.GetName()));
                        count.removeLast();
                    }
                }, new Code(){

					@Override
					public void getCode(ArrayList<ElementoPila> tokens,
							ElementoPila reduce) {
							String decs = "";
							for (Map.Entry<String, Row> entry : SLR.tabla.current.tablita.entrySet()) {
								if(entry.getValue().identity.equals(idType.var))
									decs+=("var "+entry.getKey()+";\n");
							}
							decs+=tokens.get(5).code;
							decs = decs.replaceAll("(?m)^", "   ");
							reduce.code.append("var "+tokens.get(0).name+ "= function(){\n"+decs+"}");
						
					}
                	
                });

        this.addProduction(Non_terminal.func_dec, new Symbols[]{Terminal.ID,Terminal.FUNCTION,Terminal.LPAR,Non_terminal.param_dec,Terminal.RPAR,Terminal.ENDS,Non_terminal.stalist,
                Terminal.END}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(7);
                tabla.SetCount(firstOp.GetName(), count.getLast());
                //System.out.println(tabla.GetCount(firstOp.GetName()));
                // Limpiamos el conteo
                count.removeLast();
            }
        }, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
					String decs = "";
					for (Map.Entry<String, Row> entry : SLR.tabla.current.tablita.entrySet()) {
						if(entry.getValue().identity.equals(idType.var))
							decs+="var "+entry.getKey()+";\n";
					}
					String indented =decs + tokens.get(6).code.toString();
					indented = indented.replaceAll("(?m)^", Grammar.getTabs());
					String str = "var "+tokens.get(0).name+" = function("+tokens.get(3).code+"){\n"+indented+"\n}";
					reduce.code.append(str);
			}
        	
        });

        SemanticAction plusParam = new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                int tmp = count.pollLast();
                count.add(tmp + 1);
            }
        };

        this.addProduction(Non_terminal.param_dec, new Symbols[]{Non_terminal.param_dec,Terminal.COMMA, Terminal.ID}, plusParam, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append(tokens.get(0).code);
				reduce.code.append(", "+tokens.get(2).name);
			}
        	
        });
        this.addProduction(Non_terminal.param_dec, new Symbols[]{Terminal.ID}, plusParam, appendingCode);

        this.addProduction(Non_terminal.if_s, new Symbols[]{Terminal.IF,Non_terminal.expr_bool,Terminal.THEN, Non_terminal.statement_in_line}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                //System.out.println("Simbolos -> " + simbolos.toString());
                SemanticSymbol firstOp = simbolos.get(2);
                TYPE[] accepted = new TYPE[] {TYPE.BOOLEAN,TYPE.COMODIN};
                if (!ContainsType(accepted, firstOp))
                    PrintError(firstOp.toFormat() + " -> Se esperaba un booleano");
            }
        }, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
					reduce.code.append("if("+tokens.get(1).code+") { "+tokens.get(3).code+" }");				
			}
        	
        });

        this.addProduction(Non_terminal.if_s, new Symbols[]{Terminal.IF,Non_terminal.expr_bool,Terminal.THEN,
                Non_terminal.statement_in_line,Terminal.ELSE, Non_terminal.statement_in_line}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(4);
                TYPE[] accepted = new TYPE[] {TYPE.BOOLEAN,TYPE.COMODIN};
                if (!ContainsType(accepted, firstOp))
                    PrintError(firstOp.toFormat() + " -> Se esperaba un booleano");
            }
        }, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
					reduce.code.append("if("+tokens.get(1).code+"){ "+tokens.get(3).code+" } else {"+tokens.get(5).code+"}");				
			}     	
        });

        this.addProduction(Non_terminal.if_s, new Symbols[]{Terminal.IF,Non_terminal.expr_bool,Terminal.ENDS, Non_terminal.stalist,
                Terminal.END}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(3);
                TYPE[] accepted = new TYPE[] {TYPE.BOOLEAN,TYPE.COMODIN};
                if (!ContainsType(accepted, firstOp))
                    PrintError(firstOp.toFormat() + " -> Se esperaba un booleano");
            }
        },new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
					reduce.code.append("if("+tokens.get(1).code+"){\n"+tokens.get(3).code.toString().replaceAll("(?m)^", "   ")+"}\n");
			}     	
        });

        this.addProduction(Non_terminal.if_s, new Symbols[]{Terminal.IF,Non_terminal.expr_bool,Terminal.ENDS,
                Non_terminal.stalist,Terminal.ELSE, Terminal.ENDS, Non_terminal.stalist, Terminal.END}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(6);
                TYPE[] accepted = new TYPE[] {TYPE.BOOLEAN,TYPE.COMODIN};
                if (!ContainsType(accepted, firstOp))
                    PrintError(firstOp.toFormat() + " -> Se esperaba un booleano");
            }
        }, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
					reduce.code.append("if("+tokens.get(1).code+"){\n"+tokens.get(3).code.toString().replaceAll("(?m)^", "   ")+"\n}else{\n"+tokens.get(6).code.toString().replaceAll("(?m)^", "   ")+"\n}\n");
			}     	
        });

        this.addProduction(Non_terminal.switch_s, new Symbols[]{Terminal.SWITCH, Non_terminal.expr_math, Terminal.ENDS,
                Non_terminal.switch_body, Terminal.END}, new Code(){

					@Override
					public void getCode(ArrayList<ElementoPila> tokens,
							ElementoPila reduce) {
						reduce.code.append("switch("+tokens.get(1).code+"){\n"+tokens.get(3).code+"\n}");						
					}       	
        });

        this.addProduction(Non_terminal.switch_body, new Symbols[]{Terminal.WHEN, Terminal.NUMBER, Terminal.THEN, Terminal.ENDS,
                Non_terminal.stalist, Terminal.END, Terminal.ENDS}, new Code(){
					@Override
					public void getCode(ArrayList<ElementoPila> tokens,
							ElementoPila reduce) {
						String str = "case "+tokens.get(1).name+":\n";
						reduce.code.append(str.replaceAll("(?m)^", "   "));
						String indented = tokens.get(4).code.toString()+"break;\n";
						indented=indented.replaceAll("(?m)^", "      ");
						reduce.code.append(indented);					
					}	
        });

        this.addProduction(Non_terminal.switch_body, new Symbols[]{Non_terminal.switch_body, Terminal.WHEN, Terminal.NUMBER,
                Terminal.THEN, Terminal.ENDS, Non_terminal.stalist, Terminal.END, Terminal.ENDS}, new Code(){
				@Override
				public void getCode(ArrayList<ElementoPila> tokens,
						ElementoPila reduce) {
					reduce.code.append(tokens.get(0).code);
					String str = "case "+tokens.get(2).name+":\n";
					reduce.code.append(str.replaceAll("(?m)^", "   "));
					String indented = tokens.get(5).code.toString()+"break;\n";
					indented = indented.replaceAll("(?m)^", "      ");
					reduce.code.append(indented);					
				}	
        });

        this.addProduction(Non_terminal.function_call, new Symbols[]{Terminal.ID, Terminal.LPAR, Terminal.RPAR}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(2);
                funcCall.add(firstOp.GetName(), firstOp.toFormat(), 0);
                /*
                int tmp = tabla.GetCount(firstOp.GetName());
                if (tmp != 0)
                    PrintError(firstOp.toFormat() + " -> Se esperaban " + tmp + " parámetro(s) y se recibieron 0");
                    */
            }
        }, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
					reduce.code.append(tokens.get(0).name+"()");				
			}     	
        });

        this.addProduction(Non_terminal.function_call, new Symbols[]{Terminal.ID, Terminal.LPAR, Non_terminal.param_list, Terminal.RPAR}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(3);
                funcCall.add(firstOp.GetName(), firstOp.toFormat(), simbolos.get(1).GetCount());
            }
        }, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
					reduce.code.append(tokens.get(0).name+"("+tokens.get(2).code+")");				
			}     	
        });

        this.addProduction(Non_terminal.param_list, new Symbols[]{Non_terminal.param_list, Terminal.COMMA, Non_terminal.expr_bool}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                insert.plusCount(simbolos.get(2).GetCount());
            }
        }, code_element_code);

        this.addProduction(Non_terminal.param_list, new Symbols[]{Non_terminal.expr_bool}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                insert.plusCount(simbolos.get(0).GetCount());
            }
        },upCode);

        this.addProduction(Non_terminal.cycle, new Symbols[]{Non_terminal.for_s}, upCode);
        this.addProduction(Non_terminal.cycle, new Symbols[]{Non_terminal.while_s}, upCode);

        this.addProduction(Non_terminal.for_s, new Symbols[]{Terminal.FOR, Terminal.ID, Terminal.IN, Non_terminal.rango, Terminal.ENDS, 
        		Non_terminal.stalist, Terminal.END, Terminal.ENDS}, new Code(){

					@Override
					public void getCode(ArrayList<ElementoPila> tokens,
							ElementoPila reduce) {
						String rango = tokens.get(3).code.toString();
						rango = rango.replaceAll(";", "; "+tokens.get(1).name);
						reduce.code.append("for("+tokens.get(1).name+rango+"){\n"+
						tokens.get(5).code.toString().replaceAll("(?m)^", "   ")+"\n}\n");
					}
        	
        });

        this.addProduction(Non_terminal.rango, new Symbols[]{Terminal.LCOR, Terminal.NUMBER, Terminal.RAN_2, Terminal.NUMBER, Terminal.RCOR}, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
					if(Integer.parseInt(tokens.get(1).name)< Integer.parseInt(tokens.get(3).name))
						reduce.code.append("= "+tokens.get(1).name+";"+"<="+tokens.get(3).name+";++");
					else
						reduce.code.append("= "+tokens.get(1).name+";"+">="+tokens.get(3).name+";--");
				
			}
        	
        });
        this.addProduction(Non_terminal.rango, new Symbols[]{Terminal.LCOR, Terminal.NUMBER, Terminal.RAN_3, Terminal.NUMBER, Terminal.RCOR},
        		 new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
					if(Integer.parseInt(tokens.get(1).name)< Integer.parseInt(tokens.get(3).name))
						reduce.code.append("= "+tokens.get(1).name+";"+"<"+tokens.get(3).name+";++");
					else
						reduce.code.append("= "+tokens.get(1).name+";"+">"+tokens.get(3).name+";--");
				
			}
        	
        });

        this.addProduction(Non_terminal.while_s, new Symbols[]{Terminal.WHILE, Non_terminal.expr_bool, Terminal.ENDS,
                Non_terminal.stalist, Terminal.END, Terminal.ENDS}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(4);
                TYPE[] accepted = new TYPE[] {TYPE.BOOLEAN,TYPE.COMODIN};
                if (!ContainsType(accepted, firstOp))
                    PrintError(firstOp.toFormat() + " -> Se esperaba un booleano");
            }
        }, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append("while ("+tokens.get(1).code+"){\n"+tokens.get(3).code.toString().replaceAll("(?m)^", "   ")+"\n}\n");
			}
        	
        });

        this.addProduction(Non_terminal.cycle_line, new Symbols[]{Non_terminal.statement_in_line, Terminal.FOR, Terminal.ID, Terminal.IN, Non_terminal.rango}, 
        		new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				String rango = tokens.get(4).code.toString();
				rango = rango.replaceAll(";", "; "+tokens.get(2).name);
				reduce.code.append("for("+tokens.get(2).name+rango+"){ "+tokens.get(0).code+" }\n");
			}
	
});

        SemanticAction evaluadorInLine = new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                SemanticSymbol firstOp = simbolos.get(0);
                TYPE[] accepted = new TYPE[] {TYPE.BOOLEAN,TYPE.COMODIN};
                if (!ContainsType(accepted, firstOp))
                    PrintError(firstOp.toFormat() + " -> Se esperaba un booleano");
            }
        };

        this.addProduction(Non_terminal.cycle_line, new Symbols[]{Non_terminal.statement_in_line, Terminal.WHILE,
                Non_terminal.expr_bool}, evaluadorInLine, new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append("while ("+tokens.get(2).code+") { "+tokens.get(0).code+" }");					
			}
});

        this.addProduction(Non_terminal.cycle_line, new Symbols[]{Non_terminal.statement_in_line, Terminal.UNLESS,
                Non_terminal.expr_bool}, evaluadorInLine,new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append("if(!("+tokens.get(2).code+")){"+tokens.get(0).code+"}\n");					
			}
});

        this.addProduction(Non_terminal.stalist_line, new Symbols[]{Non_terminal.statement_in_line, Terminal.DOT_COMMA, Non_terminal.stalist_line}, code_element_code);
        this.addProduction(Non_terminal.stalist_line, new Symbols[]{Non_terminal.statement_in_line, Terminal.DOT_COMMA, Non_terminal.statement_in_line}, code_element_code);
        this.addProduction(Non_terminal.stalist_line, new Symbols[]{Non_terminal.statement_in_line, Terminal.DOT_COMMA, Non_terminal.statement_in_line,Terminal.DOT_COMMA},code_element_code);

        this.addProduction(Non_terminal.return_s, new Symbols[]{Terminal.RETURN, Non_terminal.expr_bool, Terminal.ENDS},
                new SemanticAction() {
                    @Override
                    public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                        insert.SetType(simbolos.get(1).GetType());
                    }
                }, new Code(){

					@Override
					public void getCode(ArrayList<ElementoPila> tokens,
							ElementoPila reduce) {
						reduce.code.append("return "+tokens.get(1).code+";\n");
						
					}
                	
                });

        this.addProduction(Non_terminal.return_s, new Symbols[]{Terminal.RETURN, Non_terminal.expr_bool,Terminal.DOT_COMMA,
                Terminal.ENDS}, new SemanticAction() {
            @Override
            public void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert) {
                insert.SetType(simbolos.get(2).GetType());
            }
        },  new Code(){

			@Override
			public void getCode(ArrayList<ElementoPila> tokens,
					ElementoPila reduce) {
				reduce.code.append("return "+tokens.get(1).code+";\n");
				
			}
        	
        });
	}
	
	public static String getTabs(){
		String tab = "";
		for(int i=0; i<Grammar.tabCnt;i++){
			tab+="   ";
		}
		return tab;
	}
	

}



