package grammar;
import java.util.ArrayList;
import java.util.HashMap;

public class State {
	public HashMap<Symbols, Integer> transitions;
	public ArrayList <Production> rules;
	public boolean reduction = false;
	
	public State(ArrayList<Production> p){
		this.rules = p;
		this.transitions= new HashMap<Symbols, Integer>();
	}
	
	public boolean exists(Symbols s){
		if (transitions.containsKey(s))
			return true;
		return false;
	}
	
	public int getTransition(Symbols s){
		return transitions.get(s);
	}
	
	public void addTransition(Symbols key, int i){
		this.transitions.put(key, i);
	}
	
	public ArrayList<Symbols> getSymbolsToRead(){
		ArrayList <Symbols> syms = new ArrayList<Symbols>();		

		for(int i=0; i<this.rules.size();i++)
			if(!this.rules.get(i).isAllRead()){
				Symbols s = this.rules.get(i).getSymbolAtTransition();
				if(!syms.contains(s))
					syms.add(s);
			}
		return syms;
	}
	
	public ArrayList<Production> getReadProduction(){
		ArrayList <Production> syms = new ArrayList<Production>();		

		for(int i=0; i<this.rules.size();i++)
			if(this.rules.get(i).isAllRead()){
				Production p = this.rules.get(i);
				if(!syms.contains(p))
					syms.add(p);
			}
		return syms;
	}
	
	public boolean containsProduction(Production p){
		for(int i=0;i<this.rules.size();i++){
			Production temp = this.rules.get(i);
			if(p.equals(temp))
				return true;
		}		
		return false;
	}
	
	public boolean isReductionState(){		
		for(int i=0; i<this.rules.size();i++)
			if(this.rules.get(i).isAllRead())
				return true;
		return false;
	}

}
