package grammar;

import semantico.SemanticAction;

import java.util.ArrayList;
import java.util.HashMap;

public class TablaSimbolos {
	enum idType{var, funcion, array, call, parameter};
	
	public MyTable current;			//Puntero a la tabla sobre la que se está trabajando
		
	public TablaSimbolos(){
		this.current = new MyTable();
	}
	
//Agregar una tabla hija a la tabla actual.La nueva tabla se convierte en la tabla actual.
	public void addTable(){
		MyTable t = new MyTable();
		t.parent = current;
		current.children.add(t);
		current = t;
	}
//Método que sube el puntero current a la tabla padre. Sucede cuando se sale de un bloque.	
	public void upTable(){
		this.current = this.current.parent;
	}

//Método que guarda un nuevo id

	public void saveID(String id,idType t){
        if (t == idType.funcion)
            SemanticAction.count.add(0);
		this.current.addSymbol(id, t);
	}
	
	public void saveID(String id,idType t, int c){
		this.current.addSymbol(id,t,c);
}
	
//Método que verifica que no exista el id en la tabla actual o en tablas padres.
	public boolean existsID(String id){
		MyTable t = current.clone();
		while (t!=null){
			if (!t.isDeclared(id))
				t = t.parent;
			else
				return true;
		}
		
		return false;
	}

    public boolean redeclaredParemeter(String id){
        MyTable t = current.clone();
        while (t!=null){
            if (!t.isDeclared(id))
                t = t.parent;
            else
            if(t.GetType(id).equals(idType.parameter))
                return true;
            else
                t = t.parent;
        }

        return false;
    }

    public void SetCount(String name, Integer count){
        MyTable t = current.clone();
        while (t!=null){
            if (!t.isDeclared(name))
                t = t.parent;
            else {
                t.setCount(name, count);
                return;
            }
        }
        new IllegalAccessException("No se puede asignar el número de parametros a una función no declarada");
    }

    public int GetCount(String name){
        MyTable t = current.clone();
        while (t!=null){
            if (!t.isDeclared(name))
                t = t.parent;
            else {
                return t.GetCount(name);
            }
        }
        return -1;
    }

    public void SetType(String name, SemanticAction.TYPE tipo){
        MyTable t = current.clone();
        while (t!=null){
            if (!t.isDeclared(name))
                t = t.parent;
            else {
                t.SetType(name, tipo);
                return;
            }
        }
        new IllegalAccessException("No se puede asignar el tipo variable no declarada");
    }

    public SemanticAction.TYPE GetType(String name){
        MyTable t = current.clone();
        while (t!=null){
            if (!t.isDeclared(name))
                t = t.parent;
            else {
                return t.GetType(name);
            }
        }
        new IllegalAccessException("No se puede obtener el tipo variable no declarada");
        return null;
    }

	public void recorrerPreOrden(MyTable t, int tabs){
		for(int j=0;j<tabs;j++)
			System.out.print("\t");
		System.out.print(t.tablita.toString()+"\n");
		if(t.children.isEmpty())
			return;
		else{
			for(int i=0; i<t.children.size();i++)
				recorrerPreOrden(t.children.get(i),tabs+1);
		}		
	}
	
	public void printTablaSimbolos(){
		System.out.println("Tabla de Símbolos  ");
		while(this.current.parent!=null)
			this.current = this.current.parent;
		recorrerPreOrden(current.clone(),0);	
	}
	
//--------------------------------------Clase Tabla------------------------------------------------------------
//Equivale a cada una de las tablas anidadas. Una tabla por bloque
	public class MyTable{
		
		public HashMap<String,Row> tablita;	//Tabla con id's del bloque
		public MyTable parent;				//Puntero a la tabla padre
		public ArrayList<MyTable> children;	//Tablas de bloques anidados
		
		public MyTable(){
			tablita = new HashMap<String,Row>();
			parent = null;
			children = new ArrayList<MyTable>();
		}

//Sobreesritura del método clone
		@Override
		public MyTable clone(){
			MyTable t = new MyTable();
			t.tablita = this.tablita;
			t.parent = this.parent;
			t.children = this.children;
			return t;
		}
//Método para saber si un id ya esta declarado en la tabla		
		public boolean isDeclared(String s){
			return tablita.containsKey(s);
		}

        public SemanticAction.TYPE GetType(String name){ return this.tablita.get(name).tipo; }

        public void SetType(String name, SemanticAction.TYPE tipo){ this.tablita.get(name).tipo = tipo;}

        public void SetCount(String name, int count){ this.tablita.get(name).countOfList = count; }

        public Integer GetCount(String name){ return this.tablita.get(name).countOfList; }

//Metodo que agrega simbolos a la tabla
		public void addSymbol(String s, idType t){
			tablita.put(s, new Row(t));
		}

        public void addSymbol(String s, idType t, int c, MyTable context){
        tablita.put(s, new Row(t, c));
    }
		
		public void addSymbol(String s, idType t, int c){
			tablita.put(s, new Row(t,c));
		}

//Método que cambia la cuenta de parametros en caso de array o funcion
		public void setCount(String id, int c){
			Row r = tablita.get(id);
			r.countOfList = c;
			tablita.put(id,r);
			
		}
			
		//public void setType
		
//---------------------------------------------Clase que equivale a una fila de la tabla de simbolos-------------------------------		
		public class Row{
			public idType identity;		//Tipo de variable que es: normalita, array, hash,llamada a funcion
			public int countOfList;		//Valor que sirve para contar numero de elementos del array o numero de parametros de una funcion
            public SemanticAction.TYPE tipo = null;

    //Agregar el atributo de tipo {number, char, etc}

//Constructores

			public Row(idType t, int c){
				identity = t;
				countOfList = c;
			}
			
			public Row(idType t){
				identity = t;
				countOfList = 0;
			}
			
			public Row(){
				identity = null;
				countOfList = 0;
			}
//LLenar la tabla		
			public void fill(idType t,int c){
				this.identity = t;
				this.countOfList = c;
			}
			
			public void fill(idType t){
				this.identity = t;
				this.countOfList = 0;
			}
			
			public String toString(){
				return identity.toString()+"-"+countOfList;
			}
		}
	}

}
