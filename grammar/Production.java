package grammar;

public class Production {
	private Non_terminal non_term;
	private int transicion = 0;
	private Symbols[] production;
	
	public Production(){
		non_term = null;
		production = new Symbols[]{};
		
	}
	
	public Production(Non_terminal left_side,Symbols [] rule){
		this.setNonTerminal(left_side);
		this.setProduction(rule);
	}
	
	public Symbols[] getProduction(){
		return this.production;
	}
	
	public void setProduction(Symbols [] rule){
		this.production =rule;
	}
	
	public void setNonTerminal(Non_terminal left){
		this.non_term = left;
	}
	
	public Non_terminal getLeftSide(){
		return this.non_term;
	}
	
	public void addTransition(){
		this.transicion+=1;
	}
	
	public void resetTransition(){
		this.transicion=0;
	}
	
	public int getTransitionIndex(){
		return this.transicion;
	}
	
	public Symbols getSymbolAtTransition(){
		return this.production[this.getTransitionIndex()];
	}
	
	public boolean isAllRead(){
		if(this.transicion == this.production.length)
			return true;
		return false;
	}
	
	public void printProduction(){
		String str = "\t"+this.non_term+" -> ";
		for(int i=0; i<this.production.length;i++)
			str+=this.production[i]+" ";
		str+="["+this.transicion+"]";
		System.out.println(str);
	}
	
	public Production clone(){
		Production cloned = new Production (this.non_term,this.production);
		cloned.transicion = this.getTransitionIndex();
		return cloned;
	}
	
	public boolean equals(Production p){
		if(this.non_term == p.non_term)
			if(this.transicion==p.transicion)
				if(this.production.equals(p.production))
					return true;
		return false;
	}
	
	public int contains(Symbols s){
		for(int i=0;i<this.production.length;i++){
			if(s.equals(this.production[i]))
				return i;
		}
		
		return -1;
	}
	
	public String toString(){
		if(this.non_term == null){
			return "HOJA";
		}
		String s = this.non_term+"->[";
		for(int i=0; i<this.production.length;i++){
			s+=this.production[i].toString()+"  ";
		}
		s+="]";
		return s;
		
	}
}
