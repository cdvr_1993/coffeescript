package grammar;

public class Arbol {
	
	public Nodo root;
	public static StringBuilder Code;
	public Arbol(){
		this.root = null;
	}
		
	public void recorrerArbol(Nodo n, int tab){
		System.out.print("\n");
		for(int i=0;i<tab;i++){
			System.out.print("\t");
		}
		System.out.print(n.production.toString()+" "+n.elemento.name);
		if(n.children.isEmpty())
			return;
		else
			for(int i=0; i<n.children.size();i++){
				recorrerArbol(n.children.get(i),tab+1);
			
		}	
	}
	
	public void recorrerArbolCodigo(Nodo n, int tab){
		if(!n.elemento.name.equals(""))
			System.out.print(n.elemento.name+" ");
		if(n.children.isEmpty())
			return;
		else
			for(int i=0; i<n.children.size();i++){
				recorrerArbolCodigo(n.children.get(i),tab+1);
			
		}	
	}
	
	public void reducirArbol(Nodo n){
		if(n.children.isEmpty())
			return;
		
		if(n.children.size()==1){
			Nodo tmp = n.children.get(0);
			if(tmp.children.size()==0){
				n.elemento = tmp.elemento;
				n.children = tmp.children;
			}else{
				while(tmp.children.size()==1 && !tmp.children.get(0).children.isEmpty()){
					tmp = tmp.children.get(0);
				}
				n.children = tmp.children;
			}
			
		}
		for(int i=0; i<n.children.size();i++)
			reducirArbol(n.children.get(i));
	}


}
