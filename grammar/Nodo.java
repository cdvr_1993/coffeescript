package grammar;
import grammar.SLR.ElementoPila;

import java.util.ArrayList;

public class Nodo {
	Production production;
	public ElementoPila elemento;
	public ArrayList<Nodo> children;

	public Nodo(ElementoPila e){
		this.elemento = e;
		this.production = new Production();
		this.children = new ArrayList<Nodo>();
	}
	public Nodo(ElementoPila e, Production prod){
		this.elemento = e;
		this.production = prod;
		this.children = new ArrayList<Nodo>();
	}
	
	public void addChildren(ElementoPila n){
		this.children.add(new Nodo(n));
	}
	
	public void addChildren(Nodo n){
		this.children.add(n);
	}
	
	public Nodo clone(){
		Nodo n = new Nodo(this.elemento);
		n.children = this.children;
		return n;
	}
}
