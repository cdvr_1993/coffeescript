package grammar;

import java.util.LinkedList;

public class CodeGenerator {
	public static LinkedList<StringBuilder> codeBlocks= new LinkedList<StringBuilder>();;
	public static StringBuilder currentBlock= new StringBuilder();
	
	public CodeGenerator(){
		codeBlocks = new LinkedList<StringBuilder>();
		currentBlock = new StringBuilder();
	}
	
	public static void puchBlock(){
		codeBlocks.push(currentBlock);
		currentBlock = new StringBuilder();
	}
}
