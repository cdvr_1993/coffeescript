package ui;

import grammar.SLR;
import lexer.Lexer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.*;

/**
 * Created by cristian on 12/05/14.
 */
public class MWindow extends JFrame{
    JFrame MWCode = null;
    JMenuBar miMenu = new JMenuBar();
    JMenu mFile = new JMenu("Archivo");
    JMenuItem mOpen = new JMenuItem("Abrir");
    JMenuItem mSave = new JMenuItem("Guardar");
    JMenu mBuild = new JMenu("Compilar");
    JMenuItem mCompile = new JMenuItem("Compilar");
    JMenu mTools = new JMenu("Herramientas");
    JMenuItem mOut = new JMenuItem("Código generado");
    JScrollPane codeScroll = null;
    JTextArea txtCode = new JTextArea();
    JScrollPane errScroll = null;
    JTextArea txtErr = new JTextArea();
    String filename = null;
    String code = null;
    public static Boolean canGenerate = true;

    public MWindow(){
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(800, 600);
        setLocationRelativeTo(null);
        setLayout(null);
        initUI();
        initActions();
        setVisible(true);
    }

    private void initUI(){
        miMenu.add(mFile);
        miMenu.add(mBuild);
        miMenu.add(mTools);
        setJMenuBar(miMenu);
        mFile.add(mOpen);
        mFile.add(mSave);
        mBuild.add(mCompile);
        mTools.add(mOut);
        codeScroll = new JScrollPane(txtCode);
        errScroll = new JScrollPane(txtErr);
        resizeTextAreas();
        add(codeScroll);
        add(errScroll);
    }

    private void resizeTextAreas(){
        codeScroll.setLocation(10, 10);
        codeScroll.setSize(getWidth() - 20, getHeight() * 2 / 3);
        errScroll.setLocation(codeScroll.getX(), codeScroll.getY() + codeScroll.getHeight() + 10);
        errScroll.setSize(codeScroll.getWidth(), getHeight() - errScroll.getY() - 70);
    }

    private void initActions(){
        mOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Open();
            }
        });
        mSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Save("Archivo guardado con éxito");
            }
        });
        mCompile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Compile();
            }
        });
        mOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OpenCode();
            }
        });
        this.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
                resizeTextAreas();
                repaint();
            }

            @Override
            public void componentMoved(ComponentEvent e) {
            }

            @Override
            public void componentShown(ComponentEvent e) {
            }

            @Override
            public void componentHidden(ComponentEvent e) {
            }
        });
    }

    private void Open(){
        JFileChooser fSelect = new JFileChooser();
        if (fSelect.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
            File tmpFile = fSelect.getSelectedFile();
            filename = tmpFile.getAbsolutePath();
            try {
                System.out.println(filename);
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
                String tmp = null;
                txtCode.setText("");
                while ((tmp = br.readLine()) != null)
                    txtCode.append(tmp + "\n");
            }catch(Exception e){ System.err.println(e.getMessage()); }
        }
    }

    private void Save(String msg){
        if (filename == null || filename == "") return;
        try {
            OutputStreamWriter wr = new OutputStreamWriter(new FileOutputStream(filename), "UTF-8");
            wr.write(txtCode.getText());
            wr.close();
            if (msg != null)
                JOptionPane.showMessageDialog(this, msg);
        }catch (Exception e){ System.err.println(e.getMessage()); }

    }

    private void Compile(){
        if (filename == null || filename == "") return;
        Save(null);
        txtErr.setText("");
        canGenerate = true;
        try {
            Reader in = new InputStreamReader(new FileInputStream(filename), "UTF8");
            Lexer lexer = new Lexer(in);
            SLR analyzer = new SLR();
            //analyzer.readToken(lexer);
            analyzer.analyzePerToken2(lexer);
            //analyzer.canonico.printCanonico();
            //analyzer.canonico.table.simplePrint();
            //System.out.println("\n");
            //analyzer.analyze(entrada);
        }catch (Exception e){}
    }

    public void PrintError(String error){
        txtErr.append(error + "\n");
        canGenerate = false;
    }

    public void SetCode(String code){
        if (canGenerate)
            this.code = code;
    }

    private void OpenCode(){
        if (MWCode != null)
            MWCode.dispose();
        MWCode = new MWindowCodeOut(code);
        System.out.println(code);
    }

    private class MWindowCodeOut extends JFrame{
        JTextArea txtCode = new JTextArea();
        JScrollPane scrollCode = null;
        public MWindowCodeOut(String code){
            setSize(800, 600);
            setLocationRelativeTo(null);
            setLayout(new BorderLayout());
            scrollCode = new JScrollPane(txtCode);
            add(scrollCode, BorderLayout.CENTER);
            txtCode.setText(code);
            txtCode.setEditable(false);
            setVisible(true);
        }
    }
}
