package lexer;

import grammar.Symbols;
import grammar.Terminal;

/**
 * Created by cristian on 8/03/14.
 */
public class Token{
   public String value;
   public Terminal token;
   public int line;
   public int column;
   
   public Token(String v, Terminal t, int line, int column){
	   this.value = v;
	   this.token = t;
	   this.line = line;
	   this.column = column;
   }
   
   public boolean equals(Object o){
	   if(o.getClass().equals(Token.class)){
		   Token temp = (Token)o;		   
		   if(temp.value.equals(this.value))
			   if(temp.token.equals(this.token))
				   if(temp.line == this.line)
					   if(temp.column == this.column)
						   return true;
	   }
	   return false;
   }

}
