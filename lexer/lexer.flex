package lexer;
import grammar.Terminal;
%%
%class Lexer
%public
%line
%column
%type Token


/* En esta sección se declaran las macros */

ID = [a-zA-Z][a-zA-Z0-9"_"]*
INT = [0-9]+
NUMBER = {INT}("."("0"|({INT}))("E"("+"|"-")?{INT})?)?
TRUE = "true"|"yes"|"on"
FALSE = "false"|"no"|"off"
STRING = \"([^\\\"]|(\\.))*\"
CHAR = "'"."'"
RAN_2 = ".."
RAN_3 = "..."
ID_HASH = {ID}":"
TAB = [\t]
FUNCTION = "->"
ENDS = \n
AND = "&&"|"and"
OR = "||"|"or"
NOT = "!"|"not"
EQUAL = "=="|"is"
N_EQUAL = "!="|"isnt"
SPACE = " "
COMMENT = #[^\n#]*
M_COMMENT = ###[^#]*###

%{
    public String lexema;
%}

/* A continuación pasamos los TOKENS que se devolveran */
%%
/* Palabras reservadas */
"if"        { return new Token(yytext(),Terminal.IF,yyline,yycolumn); }
"else"      { return new Token(yytext(),Terminal.ELSE,yyline,yycolumn); }
"unless"    { return new Token(yytext(),Terminal.UNLESS,yyline,yycolumn); }
"for"       { return new Token(yytext(),Terminal.FOR,yyline,yycolumn); }
"while"     { return new Token(yytext(),Terminal.WHILE,yyline,yycolumn); }
"then"      { return new Token(yytext(),Terminal.THEN,yyline,yycolumn); }
"in"        { return new Token(yytext(),Terminal.IN,yyline,yycolumn); }
"when"      { return new Token(yytext(),Terminal.WHEN,yyline,yycolumn); }
"switch"    { return new Token(yytext(),Terminal.SWITCH,yyline,yycolumn); }
"return"    { return new Token(yytext(),Terminal.RETURN,yyline,yycolumn);}
"end"      { return new Token(yytext(),Terminal.END,yyline,yycolumn); }
{FUNCTION}  { return new Token(yytext(),Terminal.FUNCTION,yyline,yycolumn); }

/* Operadores */
";"         { return new Token(yytext(),Terminal.DOT_COMMA,yyline,yycolumn); }
"//"        { return new Token(yytext(),Terminal.INT_DIVIDE,yyline,yycolumn);}
"**"        { return new Token(yytext(),Terminal.POW,yyline,yycolumn); }
"%%"        { return new Token(yytext(),Terminal.TRUE_MOD,yyline,yycolumn); }
{EQUAL}     { return new Token(yytext(),Terminal.EQUAL,yyline,yycolumn); }
{N_EQUAL}   { return new Token(yytext(),Terminal.N_EQUAL,yyline,yycolumn); }
"<"         { return new Token(yytext(),Terminal.LESS,yyline,yycolumn); }
"<="        { return new Token(yytext(),Terminal.LESS_EQ,yyline,yycolumn); }
">"         { return new Token(yytext(),Terminal.GREATER,yyline,yycolumn); }
">="        { return new Token(yytext(),Terminal.GREATER_EQ,yyline,yycolumn); }
"="         { return new Token(yytext(),Terminal.ASSIGN,yyline,yycolumn);}
"+="        { return new Token(yytext(),Terminal.PLUS_AS,yyline,yycolumn); }
"-="        { return new Token(yytext(),Terminal.MINUS_AS,yyline,yycolumn); }
"*="        { return new Token(yytext(),Terminal.TIMES_AS,yyline,yycolumn); }
"/="        { return new Token(yytext(),Terminal.DIVIDE_AS,yyline,yycolumn); }
"("         { return new Token(yytext(),Terminal.LPAR,yyline,yycolumn);}
")"         { return new Token(yytext(),Terminal.RPAR,yyline,yycolumn); }
{TAB}       {}
{SPACE}     {}
","         { return new Token(yytext(),Terminal.COMMA,yyline,yycolumn); }
"+"         { return new Token(yytext(),Terminal.PLUS,yyline,yycolumn); }
"-"         { return new Token(yytext(),Terminal.MINUS,yyline,yycolumn);  }
"*"         { return new Token(yytext(),Terminal.TIMES,yyline,yycolumn);  }
"/"         { return new Token(yytext(),Terminal.DIVIDE,yyline,yycolumn);  }
{AND}       { return new Token(yytext(),Terminal.AND,yyline,yycolumn); }
{OR}        { return new Token(yytext(),Terminal.OR,yyline,yycolumn);  }
{NOT}       { return new Token(yytext(),Terminal.NOT,yyline,yycolumn);  }
"%"         { return new Token(yytext(),Terminal.MOD,yyline,yycolumn);  }
{M_COMMENT} { return new Token(yytext(),Terminal.M_COMMENT,yyline,yycolumn);  }
{COMMENT}   { return new Token(yytext(),Terminal.COMMENT,yyline,yycolumn);  }
{RAN_3}     { return new Token(yytext(),Terminal.RAN_3,yyline,yycolumn);  }
{RAN_2}     { return new Token(yytext(),Terminal.RAN_2,yyline,yycolumn);  }
"["         { return new Token(yytext(),Terminal.LCOR,yyline,yycolumn);  }
"]"         { return new Token(yytext(),Terminal.RCOR,yyline,yycolumn);  }
"?"         { return new Token(yytext(),Terminal.EXIST,yyline,yycolumn);  }
"{"         { return new Token(yytext(),Terminal.LKEY,yyline,yycolumn);  }
"}"         { return new Token(yytext(),Terminal.RKEY,yyline,yycolumn);  }
"."         { return new Token(yytext(),Terminal.DOT,yyline,yycolumn);  }

/* Continuación de las declaraciones */
{NUMBER}    { return new Token(yytext(),Terminal.NUMBER,yyline,yycolumn); }
{TRUE}      { return new Token(yytext(),Terminal.TRUE,yyline,yycolumn);  }
{FALSE}     { return new Token(yytext(),Terminal.FALSE,yyline,yycolumn);  }
{STRING}    { return new Token(yytext(),Terminal.STRING,yyline,yycolumn);  }
{CHAR}      { return new Token(yytext(),Terminal.CHAR,yyline,yycolumn); }
{ID}        { return new Token(yytext(),Terminal.ID,yyline,yycolumn);  }
{ENDS}      { return new Token(yytext(),Terminal.ENDS,yyline,yycolumn);  }
.           { return new Token(yytext(),Terminal.ERROR,yyline,yycolumn);  }