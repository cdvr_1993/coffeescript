package semantico;

import grammar.Symbols;
import grammar.TablaSimbolos;

/**
 * Created by cristian on 27/04/14.
 */
public class SemanticSymbol {
    public Symbols simbolo = null;
    private SemanticAction.TYPE tipo = null;
    private int line = 0, column = 0;
    private String name = null;
    private Integer count = 0;


    public SemanticSymbol (Symbols simbolo){ this.simbolo = simbolo; }

    public SemanticSymbol (Symbols simbolo, int line, int column){ this.simbolo = simbolo; this.line = line; this.column = column; }

    public SemanticAction.TYPE GetType(){
        return this.tipo;
    }

    public void SetType(SemanticAction.TYPE tmp){
        this.tipo = tmp;
    }

    public int GetLine(){
        return this.line;
    }

    public int GetColumn(){
        return this.column;
    }

    public void SetLine(int line){
        this.line = line;
    }

    public void SetColumn(int column){
        this.column = column;
    }

    public String toFormat(){
        return "[" + (this.line+1) + ":" + (this.column+1) + "]";
    }

    public void plusCount(Integer i){ this.count = i + 1; }

    public Integer GetCount(){ return this.count; }

    public String GetName(){ return this.name; }

    public void SetName(String name){ this.name = name; }

    public String toString(){
        if (tipo == null) return "null";
        return tipo.toString();
    }
}
