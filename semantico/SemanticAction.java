package semantico;

import driver.Driver;
import grammar.TablaSimbolos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by cristian on 24/04/14.
 */
public abstract class SemanticAction {
    /* Es la acción que se realizara, el primero token sera el de la producción para asignarle el tipo,
    * el segundo token sera el primer operador, y el segundo token sera el segundo operador en caso de existir */

    public enum TYPE {STRING, NUMBER, BOOLEAN, CHAR, COMODIN}
    public static TablaSimbolos tabla = null;
    public static LinkedList<Integer> count = new LinkedList<Integer>();
    public static FunctionCall funcCall = new FunctionCall();

    public boolean ContainsType(TYPE[] array, SemanticSymbol find){
        for(TYPE tmp : array)
            if (tmp == find.GetType()) return true;
        return false;
    }

    public static void PrintError(String error){ Driver.miVentana.PrintError("Warning: " + error); }

    public abstract void Execute(LinkedList<SemanticSymbol> simbolos, SemanticSymbol insert);

    public static class FunctionCall{
        LinkedList<Row> list = new LinkedList<Row>();

        public void add(String name, String lineColumn, int paramSend){
            list.add(new Row(name, lineColumn, paramSend));
        }

        public void CheckFuncCalls(){
            for (Row tmp : list){
                TablaSimbolos.MyTable tmpCursor = tmp.context.clone();
                boolean find = false;
                while (tmpCursor!=null){
                    if (!tmpCursor.isDeclared(tmp.name))
                        tmpCursor = tmpCursor.parent;
                    else{
                        int paramDec = tmpCursor.GetCount(tmp.name);
                        if (paramDec != tmp.count)
                            PrintError(tmp.lineColumn + " -> Se esperaban " + paramDec + " parámetro(s) y se recibieron " + tmp.count);
                        find = true;
                        break;
                    }
                }
                if (!find)
                    PrintError(tmp.lineColumn + " -> Función " + tmp.name + " no declarada en este contexto");
            }
        }

        private static class Row {
            TablaSimbolos.MyTable context = null;
            String name = null;
            int count = -1;
            String lineColumn = null;

            public Row(String name, String lineColumn, Integer count){
                this.context = tabla.current.clone();
                //System.out.println("Declarada " + name + " -> " + this.context.isDeclared(name));
                this.name = name;
                this.lineColumn = lineColumn;
                this.count = count;
            }
        }
    }
}
