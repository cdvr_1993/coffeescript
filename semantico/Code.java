package semantico;

import grammar.SLR.ElementoPila;

import java.util.ArrayList;

public abstract class Code {
	public abstract void getCode(ArrayList<ElementoPila> tokens, ElementoPila reduce);
	
}
